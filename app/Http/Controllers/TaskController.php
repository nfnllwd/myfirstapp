<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Task;


class TaskController extends Controller{

	public function recieve() {

		$user = Auth::user();
		if (!Auth::check()) {
			return redirect(url('/auth/login'));
		}
	    $tasks = Task::orderBy('created_at', 'asc')->where('ownerID', $user->id)->get();

	    return view('tasks', [
	        'tasks' => $tasks
	    ]);
	}

	public function store(Request $request){

		$user = Auth::user();

		$validator = Validator::make($request->all(),[
			'name' => 'required|max:255',
		]);
		if ($validator->fails()) {
			return redirect(url('/'))
				->withInput()
				->withErrors($validator);
		}
		$task = new Task;
		$task->name = $request->name;
		$task->ownerID = $user->id;
		$task->save();
		return redirect(url('/'));
	}	

	public function destroy($id) {

  		Task::findOrFail($id)->delete();

  		return redirect(url('/'));
	}
}
