<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\User;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Hash;
use Session;


class UsersController extends Controller{

	public function getLogin(){
		return view('auth/login');
	}

	public function authentificate(Request $request){
		$user = array(
			'email' => $request->email,
			'password' => $request->password//Hash::make($request->password)
		);

		if (Auth::attempt($user,$request->remember)) {
			return redirect(url('/'));
		} 
		Session::flash('auth_fail_message', 'Smth wrong. Check it');
		return (view('auth/login'));
	}


	public function getLogout(){
		Auth::logout();
		return redirect(url('auth/login'));
	}

	public function getRegister(){
		return view('auth/register');
	}

	public function postRegister(Request $request){
		
		$validator = Validator::make($request->all(),[
			'name' => 'required|max:30',
			'email' => 'required|unique:users|max:50|email',
			'date' => 'required|date',
			'password' => 'required|max:40',
			'password_confirmation' => 'required|same:password',
		]);

		if ($validator->fails()) {
			Session::flash('register_fail_message', 'There is a mistake and U should fix it!');
			return view('auth/register');
		}

		$newUser = array(
			'name' => $request->name,
			'email' => $request->email,
			'dateOfBirth' => $request->date,
			'password' => Hash::make($request->password)
		);

		User::insert($newUser);
		
		Auth::attempt(['email' => $request->email, 'password' => $request->password]);

		return redirect(url('/'));
	}

	public function getSettings(){
		if (!Auth::check()) {
			return redirect(url('/auth/login'));
		}
		$user = Auth::user();
		return view('user/Settings',['name' => $user->name, 'about' => $user->about]);
	}

	public function postSettings(Request $request){
		$user  = Auth::user();

		$validator = Validator::make($request->all(),[
			'name' => 'required|max:30',
			'password' => 'max:40',
			'password_confirmation' => 'same:password',
			'about' => 'max:255',
		]);

		if ($validator->fails()) {
			Session::flash('settings_fail_message', 'There is a mistake and U should fix it!');
			return view('user/settings',['name' => $user->name, 'about' => $user->about]);
		}
		if ($request->password != "") {
			User::where('email', $user->email)->update(['name'=>$request->name,
		 		'password'=> Hash::make($request->password),
		 		'about'=>$request->about]);
		} else {
			User::where('email', $user->email)->update(['name'=>$request->name,
		 		'about'=>$request->about]);
		}
		return redirect(url('/'));
	}	
}