<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Task;
use Illuminate\Http\Request;

/*Show task*/
Route::get('/','TaskController@recieve');

/*Add n3w*/

Route::post('/task', 'TaskController@store');

/*Delete*/

Route::delete('/task/{id}', 'TaskController@destroy');




// Маршруты аутентификации...
Route::get('auth/login', 'UsersController@getLogin');
Route::post('auth/login', 'UsersController@authentificate');
Route::get('auth/logout', 'UsersController@getLogout');

// Маршруты регистрации...
Route::get('auth/register', 'UsersController@getRegister');
Route::post('auth/register', 'UsersController@postRegister');

//pass reset r0utes
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

Route::get('user/settings', 'UsersController@getSettings');
Route::post('user/settings', 'UsersController@postSettings');