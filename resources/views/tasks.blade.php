@extends('layouts.app')

@section('content')

    <div class="panel-body">
    
        <!--Errs-->
        @include('common.errors')
        
        <!--New task-->
        <form action="{{ url('/task') }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            
            <!--Task name-->
            <div class="form-group">
                <label for="task" class="col-sm-3 control-label">Add new task</label>
                
                <div class="col-sm-6">
                    <input type="text" name="name" id="task-name" class="form-control"> 
                </div>
            </div>

            <!--Task Button-->
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6"> 
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Add Task
                    </button>
                </div>
            </div>
        </form>
    </div>

    <!--Current tasks here-->
    @if (count($tasks) > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                Current Tasks:
            </div>

            <div class="panel-body">
                <table class="table table-striped task-table">
                    
                    <!-- tbl headings-->
                    <thead>
                       <!-- <th>Task</th>
                        <th>&nbsp</th>
                    </thead>

                    <!--tbl body-->
                    <tbody>
                        @foreach ($tasks as $task)
                            <tr class="taskline">
                                    <!--D31337 buttn</!-->
                                    <td>
                                        <form action="{{ url('task/'.$task->id)}}" method="POST">
                                            {{ csrf_field()}}
                                            {{ method_field('DELETE')}}

                                            <button type="submit" class="btn btn-danger">
                                                <i class="fa fa-trash"></i> Delete
                                            </button>
                                        </form>
                                    </td>

                                    <!--Task name</!-->
                                    <td class="table-text">
                                        <div>{{$task->name}}</div>
                                    </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection